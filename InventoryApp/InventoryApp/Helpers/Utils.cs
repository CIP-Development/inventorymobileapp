﻿using InventoryApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace InventoryApp.Helpers
{
    static class Utils
    {
        public static void RefreshLang(Object obj, IDataStoreService dataStoreService, string formName)
        {
            if (dataStoreService.AppLangResourceList != null)
            {
                PropertyInfo[] props = obj.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name.StartsWith("Ux", StringComparison.Ordinal))
                    {
                        prop.SetValue(obj, dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, formName, prop.Name));
                    }
                }
            }

            /*if (_dataStoreService.AppLangResourceList != null)
            {
                PropertyInfo[] props = this.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    if(prop.Name.StartsWith("Ux", StringComparison.Ordinal))
                    {
                        prop.SetValue(this, _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "WelcomePage", prop.Name));
                    }
                }
            }*/
        }
    }
}
