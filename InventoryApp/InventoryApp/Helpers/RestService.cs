﻿using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class RestService : IRestService
    {
        readonly HttpClient _httpClient;
        public List<InventoryViability> InventoryViabilityItems { get; private set; }

        private const string SearchEndPoint = "://{0}/GringlobalService/WCFService.svc/search/{1}?dataview={2}";
        private const string RestUrlC = "://{0}/GringlobalService/WCFService.svc/rest/{1}";
        private const string RestUrlRUD = "://{0}/GringlobalService/WCFService.svc/rest/{1}/{2}";
        private const string InventoryViabilityListUrl = "://{0}/GringlobalService/WCFService.svc/getdata/get_mob_inventory_viability_list";
        private const string GetDataURL = "://{0}/GringlobalService/WCFService.svc/getdata/{1}";
        private const string GetDataEndPoint = "://{0}/GringlobalService/WCFService.svc/getdata/{1}?parameters={2}";

        public RestService()
        {
            //workaround for android ssl validation bug
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };
            _httpClient = new HttpClient(httpClientHandler);
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.MaxResponseContentBufferSize = 256000;
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Settings.UserToken);

            //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //_client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("utf-8"));
            //_client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.Token));

            //specify to use TLS 1.2 as default connection
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        #region InventoryViability
        public async Task<string> CreateInventoryViabilityAsync(InventoryViability item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "inventory_viability");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> UpdateInventoryViabilityAsync(InventoryViability item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory_viability", item.InventoryViabilityId);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public Task DeleteInventoryViabilityAsync(string id)
        {
            throw new NotImplementedException();
        }
        public Task SaveInventoryViability(InventoryViability item, bool isNewItem)
        {
            throw new NotImplementedException();
        }
        public async Task<List<InventoryViability>> RefreshInventoryViabilityAsync()
        {
            InventoryViabilityItems = new List<InventoryViability>();

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(InventoryViabilityListUrl, Settings.Server);

            try
            {
                var response = await _httpClient.GetAsync(URL);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    InventoryViabilityItems = JsonConvert.DeserializeObject<List<InventoryViability>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return InventoryViabilityItems;
        }
        #endregion

        public async Task<List<InventoryViability>> SearchInventoryViabilityAsync(string query, string dataview, string resolver)
        {

            List<InventoryViability> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task<List<InventoryThumbnail>> SearchInventoryThumbnailAsync(string query, string dataview, string resolver)
        {
            List<InventoryThumbnail> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryThumbnail>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        #region InventoryViabilityData
        public async Task<List<InventoryViabilityData>> SearchInventoryViabilityDataAsync(string query, string dataview, string resolver)
        {
            List<InventoryViabilityData> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViabilityData>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = new List<InventoryViabilityData>();
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }
            return result;
        }

        public async Task<string> CreateInventoryViabilityDataAsync(InventoryViabilityData item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "inventory_viability_data");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        public async Task<string> UpdateInventoryViabilityDataAsync(InventoryViabilityData item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory_viability_data", item.InventoryViabilityDataId);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }

        public async Task<List<ViabilityOrderItem>> GetViabilityRequestItemList(string orderRequestId)
        {
            List<ViabilityOrderItem> result = null;

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(GetDataEndPoint, Settings.Server, "get_mob_viability_request_item_by_order_request_id", 
                System.Net.WebUtility.UrlEncode(":orderrequestid=" + orderRequestId + ";:syslangid=" + Settings.LangId));
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<ViabilityOrderItem>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = new List<ViabilityOrderItem>();
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        public async Task<ViabilityOrderItem> ReadViabilityRequestItem(int orderRequestItemId)
        {
            ViabilityOrderItem result = null;

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(GetDataEndPoint, Settings.Server, "get_mob_viability_request_item", 
                System.Net.WebUtility.UrlEncode(":orderrequestitemid=" + orderRequestItemId + ";:syslangid=" + Settings.LangId));
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<ViabilityOrderItem>>(resultContent);
                if (list != null && list.Count > 0)
                {
                    result = list.FirstOrDefault();
                }
                else
                    throw new Exception("Order request not found");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception("Order request not found");
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        #endregion

        #region OrderRequestItem
        public async Task<string> UpdateOrderItemRequest(ViabilityOrderItem orderItem)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderItem));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "order_request_item", orderItem.order_request_item_id);
            var response = await _httpClient.PutAsync(URL, content) ;

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        #endregion

        #region OrderRequestItemAction
        public async Task<string> CreateOrderRequestItemActionAsync(OrderRequestItemAction orderRequestItemAction)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequestItemAction));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "order_request_item_action");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion
        public async Task<List<InventoryViabilityRule>> SearchInventoryViabilityRuleAsync(string query, string dataview, string resolver)
        {
            List<InventoryViabilityRule> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViabilityRule>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }

        public async Task<string> UpdateInventoryAsync(Inventory item)
        {
            string result = string.Empty;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory", item.inventory_id);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }

        public async Task<List<CodeValueLookup>> GetCodeValueLookupList(DateTime modifiedDate)
        {
            List<CodeValueLookup> result = null;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = @"http://{0}/GringlobalService/WCFService.svc/getdata/{1}?parameters={2}";
            URL = string.Format(URL, Settings.Server, "mob_code_value_lookup", ":createddate;:modifieddate");
            //URL = @"http://192.168.137.1/GringlobalService/WCFService.svc/getdata/mob_code_value_lookup?parameters=:createddate;:modifieddate";
            //URL = @"http://192.168.137.1/GringlobalService/WCFService.svc/getdata/mob_code_value_by_groupname?parameters=:groupname=ACCESSION_NAME_TYPE";
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<CodeValueLookup>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }

    }

    public interface RestApi<T, in Tkey> where T : class
    {

    }

    public class RestClass<T, Tkey> where T : class
    {
        readonly HttpClient _client;
        private const string InventoryViabilityListUrl = "http://{0}/GrinGlobalService/WCFService.svc/getdata/get_mob_inventory_viability_list";

        public RestClass(HttpClient client)
        {
            _client = client;
        }

        public T Entity { get; set; }
        /*
        public async Task<T> CreateAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> ReadAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> UpdateAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> DeleteAsync(Tkey id)
        {
            return Entity;
        }
        */
    }
}
