﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace InventoryApp.ViewModels
{
    public class PrintInventoryViabilityPageViewModel : ViewModelBaseZ
    {
        #region Properties
        private Printer _printer;
        public Printer Printer
        {
            get { return _printer; }
            set { SetProperty(ref _printer, value); }
        }

        private int _rowsPerRecord;
        public int RowsPerRecord
        {
            get { return _rowsPerRecord; }
            set { SetProperty(ref _rowsPerRecord, value); }
        }

        private LabelTemplate _labelTemplate;
        public LabelTemplate LabelTemplate
        {
            get { return _labelTemplate; }
            set { SetProperty(ref _labelTemplate, value); }
        }

        private List<Printer> _printerList;
        public List<Printer> PrinterList
        {
            get { return _printerList; }
            set { SetProperty(ref _printerList, value); }
        }

        private List<LabelTemplate> _labelTemplateList;
        public List<LabelTemplate> LabelTemplateList
        {
            get { return _labelTemplateList; }
            set { SetProperty(ref _labelTemplateList, value); }
        }

        private string _printingLog;
        public string PrintingLog
        {
            get { return _printingLog; }
            set { SetProperty(ref _printingLog, value); }
        }

        private bool _isLogVisible;
        public bool IsLogVisible
        {
            get { return _isLogVisible; }
            set { SetProperty(ref _isLogVisible, value); }
        }

        private string _orderBy;
        public string OrderBy
        {
            get { return _orderBy; }
            set { SetProperty(ref _orderBy, value); }
        }

        private bool _createInventoryAction;
        public bool CreateInventoryAction
        {
            get { return _createInventoryAction; }
            set { SetProperty(ref _createInventoryAction, value); }
        }
        #endregion
        
        private readonly RestClient _restClient;
        private List<ViabilityOrderItem> _inventoryList;
        private int _labelIndex;
        private int _endRecordIndex;
        private List<string> _labelVariables = new List<string>();
        private IDataStoreService _dataStoreService;

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxLabelPrinter;
        public string UxLabelPrinter
        {
            get { return _UxLabelPrinter; }
            set { SetProperty(ref _UxLabelPrinter, value); }
        }
        private string _UxLabelLabelDesign;
        public string UxLabelLabelDesign
        {
            get { return _UxLabelLabelDesign; }
            set { SetProperty(ref _UxLabelLabelDesign, value); }
        }
        private string _UxLabelShowPrintingLog;
        public string UxLabelShowPrintingLog
        {
            get { return _UxLabelShowPrintingLog; }
            set { SetProperty(ref _UxLabelShowPrintingLog, value); }
        }
        private string _UxButtonCancel;
        public string UxButtonCancel
        {
            get { return _UxButtonCancel; }
            set { SetProperty(ref _UxButtonCancel, value); }
        }
        private string _UxButtonPrint;
        public string UxButtonPrint
        {
            get { return _UxButtonPrint; }
            set { SetProperty(ref _UxButtonPrint, value); }
        }

        #endregion
        public PrintInventoryViabilityPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDialogService dialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;
            _restClient = new RestClient();

            Title = "Print viability test";

            RowsPerRecord = 1;
            IsLogVisible = false;

            CancelCommand = new DelegateCommand(OnCancelCommandExecuted);
            PrintCommand = new DelegateCommand(OnPrintCommandExecuted);
            LabelTemplatetChangedCommand = new DelegateCommand(OnLabelTemplatetChangedCommand);
        }
        public DelegateCommand CancelCommand { get; }
        public DelegateCommand PrintCommand { get; }
        public DelegateCommand LabelTemplatetChangedCommand { get; }
        private async void OnLabelTemplatetChangedCommand()
        {
            try
            {
                if (LabelTemplate != null)
                {
                    string labelInfo = JsonConvert.SerializeObject(LabelTemplate, Formatting.Indented);
                    PrintingLog = "Printer URI:\n" + Printer.PrinterUri + "\n\n" + labelInfo;

                    _labelVariables.Clear();

                    Regex regex = new Regex("##" + "(.)*" + "##");
                    var matches = regex.Matches(LabelTemplate.Zpl);
                    foreach (var match in matches)
                    {
                        _labelVariables.Add(match.ToString()); //match.ToString().Replace(startMark,"").Replace(endMark,"");
                    }
                }
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private async void OnPrintCommandExecuted()
        {
            try
            {
                if (Printer == null)
                    throw new Exception("Printer is empty");
                if (LabelTemplate == null)
                    throw new Exception("Label template is empty");
                if (_inventoryList == null || _inventoryList.Count == 0)
                    throw new Exception("Printing inventory list is empty");

                Settings.Printer = Printer.PrinterName;

                var idList = _inventoryList.Select(x => x.inventory_id).ToList();
                var dataview = await _restClient.GetDataview(LabelTemplate.Dataview, ":accessionid;:accessioninvgroupid;:inventorymaintpolicyid;:orderrequestid;:inventoryid=" + string.Join(",", idList));

                _labelIndex = 0;
                //_startRecordIndex = 0;
                _endRecordIndex = dataview.Count - 1;

                string zpl = GenerateZPL(dataview);
                PrintingLog = "Printer URI:\n" + Printer.PrinterUri + "\n\n" + zpl;
                /*
                switch (OrderBy)
                {
                    case "Accession Number":
                        _inventoryList = _inventoryList.OrderBy(x => x.AccessionNumber).ToList();
                        break;
                    case "Inventory Id":
                        _inventoryList = _inventoryList.OrderBy(x => x.inventory_id).ToList();
                        break;
                    case "Lot Id":
                        _inventoryList = _inventoryList.OrderBy(x => x.inventory_number_part2).ToList();
                        break;
                    case "Collecting Number":
                        _inventoryList = _inventoryList.OrderBy(x => x.acc_name_col).ToList();
                        break;
                    case "Accession Name":
                        _inventoryList = _inventoryList.OrderBy(x => x.acc_name_cul).ToList();
                        break;
                    default:
                        break;
                }
                */

                //await _restClient.Print(Printer.PrinterUri, Printer.PrinterConnectionType, zpl);

                await PageDialogService.DisplayAlertAsync("Printing Page", "Successfully printed", "OK");
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        private async void OnCancelCommandExecuted()
        {
            await NavigationService.GoBackAsync();
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "PrintInventoryViabilityPage");

                if (PrinterList == null)
                {
                    PrinterList = await _restClient.GetPrinterList();
                }
                if (Printer == null)
                {
                    Printer = PrinterList.FirstOrDefault(p => Settings.Printer.Equals(p.PrinterName));
                    if (Printer == null)
                        Settings.Printer = string.Empty;
                }
                if (LabelTemplateList == null)
                {
                    var filter = _dataStoreService.GetSetting(Settings.WorkgroupId, "gui_settings", "viability_label_templates");
                    LabelTemplateList = await _restClient.GetViabilityLabelTemplateList(filter);
                }

                if (parameters.ContainsKey("ViabilityRequestItemList"))
                {
                    _inventoryList = (List<ViabilityOrderItem>)parameters["ViabilityRequestItemList"];
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        private string GenerateZPL(Newtonsoft.Json.Linq.JArray dataview)
        {
            string zpl = string.Empty;

            int printerDPI = LabelTemplate.Density;
            float labelWidth = (float)LabelTemplate.Width * printerDPI;
            float labelHeight = (float)LabelTemplate.Height * printerDPI;

            float marginLeft = (float)LabelTemplate.MarginLeft * printerDPI;
            float marginTop = (float)LabelTemplate.MarginTop * printerDPI;
            float horizontalGap = (float)LabelTemplate.HorizontalGap * printerDPI;
            float verticalGap = (float)LabelTemplate.VerticalGap * printerDPI;

            int labelsPerRecord = (int)RowsPerRecord;

            int columnsPerPage = (int)LabelTemplate.HorizontalCount;
            int rowsPerPage = 0;

            float paperHeight = (float)LabelTemplate.PaperHeight * printerDPI;

            if (labelHeight > paperHeight)
            {
                rowsPerPage = 1;
            }
            else if (2 * labelHeight + verticalGap > paperHeight) // 1 row
            {
                rowsPerPage = 1;
            }
            else //more than 1 row
            {
                rowsPerPage = 1 + (int)((paperHeight - labelHeight) / (labelHeight + verticalGap));
            }

            // Generate zpl
            string templateContent = LabelTemplate.Zpl.Replace("^XA", "").Replace("^XZ", "").Replace("\r\n\r\n", "\r\n").Trim();
            int zplX = 0, zplY = 0;
            int recordIndex = (_labelIndex / labelsPerRecord);

            while (recordIndex <= _endRecordIndex)
            {
                zpl += "^XA\n";
                for (int iRow = 0; iRow < rowsPerPage; iRow++)
                {
                    zplY = (int)(marginTop + iRow * (labelHeight + verticalGap));
                    for (int iCol = 0; iCol < columnsPerPage; iCol++)
                    {
                        zplX = (int)(marginLeft + iCol * (labelWidth + horizontalGap));
                        if (recordIndex <= _endRecordIndex)
                        {
                            zpl += $"^LH{zplX},{zplY}\n";
                            string label = ReplaceVariables(templateContent, dataview[recordIndex]);
                            zpl += label + "\n";

                            _labelIndex++;
                            recordIndex = (_labelIndex / labelsPerRecord);
                        }
                        else
                            break;
                    }
                    if (recordIndex > _endRecordIndex)
                        break;
                }
                zpl += "^XZ";
            }

            return zpl;
        }

        private string ReplaceVariables(string template, Newtonsoft.Json.Linq.JToken dgrCurrent)
        {
            string label = template;
            string startMark = @"##";
            string endMark = @"##";
            foreach (var variable in _labelVariables)
            {
                var variableName = variable.Replace(startMark, "").Replace(endMark, "");
                if (null != dgrCurrent[variableName])
                {
                    string formattedVlur = dgrCurrent[variableName].ToString();
                    label = label.Replace(variable, formattedVlur.Replace("~", @"\7E").Replace("^", @"\5E")
                        .Replace("á", @"\A0")
                        .Replace("é", @"\82")
                        .Replace("í", @"\A1")
                        .Replace("ó", @"\A2")
                        .Replace("ú", @"\A3")
                        .Replace("ñ", @"\A4")
                        .Replace("Ñ", @"\A5")
                        .Replace("ü", @"\81"));
                }
            }
            return label;
        }
    }
}
