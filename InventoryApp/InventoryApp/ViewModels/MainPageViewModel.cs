﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class MainPageViewModel : ViewModelBaseZ
    {
        private IDataStoreService _dataStoreService;

        private List<MenuItem> _menuList;
        public List<MenuItem> MenuList
        {
            get { return _menuList; }
            set { SetProperty(ref _menuList, value); }
        }

        private MenuItem _selectedMenu;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenu; }
            set { SetProperty(ref _selectedMenu, value); }
        }

        #region Lang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxLabelWelcomeUser;
        public string UxLabelWelcomeUser
        {
            get { return _UxLabelWelcomeUser; }
            set { SetProperty(ref _UxLabelWelcomeUser, value); }
        }
        private string _UxLabelConnectedTo;
        public string UxLabelConnectedTo
        {
            get { return _UxLabelConnectedTo; }
            set { SetProperty(ref _UxLabelConnectedTo, value); }
        }
        private string _UxButtonLogout;
        public string UxButtonLogout
        {
            get { return _UxButtonLogout; }
            set { SetProperty(ref _UxButtonLogout, value); }
        }
        #endregion
        public MainPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;

            MenuList = new List<MenuItem> { 
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelHome"), Path = "NavigationPage/WelcomePage"} ,
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelInventoryList"), Path = "NavigationPage/InventoriesPage" },
                /*new MenuItem { Text = "Dataviews", Path = "NavigationPage/WelcomePage"},*/
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelViabilityTest"), Path = "NavigationPage/SearchInventoryViabilityPage" },
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelSettings"), Path = "NavigationPage/SettingsPage" }
                /*new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelAbout"), Path = "NavigationPage/About" }*/
            };

            NavigateCommand = new DelegateCommand<string>(OnNavigateCommandExecuted);
            LogoutCommand = new DelegateCommand(OnLogoutCommandExecuted);

            ItemTappedCommand = new DelegateCommand(OnItemTappedCommandExecuted);

            UxLabelWelcomeUser = "Welcome {0}";
            UxLabelConnectedTo = "Connected to {0}";
        }

        public DelegateCommand ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted()
        {
            if (!string.IsNullOrEmpty(SelectedMenuItem.Path))
            {
                await NavigationService.NavigateAsync(SelectedMenuItem.Path, null);
            }
        }

        public DelegateCommand<string> NavigateCommand { get; }
        private async void OnNavigateCommandExecuted(string path)
        {
            await NavigationService.NavigateAsync(path);
        }
        public DelegateCommand LogoutCommand { get; }
        private async void OnLogoutCommandExecuted()
        {
            try
            {
                _dataStoreService.InventoryList = null;
                await NavigationService.NavigateAsync("/LoginPage");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedMenuItemIndex") && parameters["SelectedMenuItemIndex"] != null && !string.IsNullOrWhiteSpace(parameters["SelectedMenuItemIndex"].ToString()))
            {
                int selectedHomeMenuItemIndex = int.TryParse(parameters["SelectedMenuItemIndex"].ToString(), out selectedHomeMenuItemIndex) ? selectedHomeMenuItemIndex : -1;
                if (selectedHomeMenuItemIndex > -1 && selectedHomeMenuItemIndex < _menuList.Count)
                    SelectedMenuItem = _menuList[selectedHomeMenuItemIndex];
            }
            SelectedMenuItem = MenuList.First();

            Utils.RefreshLang(this, _dataStoreService, "MainPage");
            UxLabelWelcomeUser = string.Format(UxLabelWelcomeUser, Settings.Username);
            UxLabelConnectedTo = string.Format(UxLabelConnectedTo, Settings.Server);
        }
    }

    public class MenuItem
    {
        public string Text { get; set; }
        public string Path { get; set; }
    }
}
