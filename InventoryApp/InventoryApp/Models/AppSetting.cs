﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class AppSetting
    {
        public string setting_name { get; set; }
        public string setting_value { get; set; }
        public string title { get; set; }
        public int lang_id { get; set; }

    }
}
