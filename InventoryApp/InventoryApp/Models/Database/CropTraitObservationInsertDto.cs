﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.Database
{
    public class CropTraitObservationInsertDto
    {
        public int crop_trait_observation_id { get; set; }
        public int inventory_id { get; set; }
        public int crop_trait_id { get; set; }
        public int? crop_trait_code_id { get; set; }
        public decimal? numeric_value { get; set; }
        public string string_value { get; set; }
        public int method_id { get; set; }
        public string is_archived { get; set; }
        public DateTime created_date { get; set; }
        public int created_by { get; set; }
        public DateTime owned_date { get; set; }
        public int owned_by { get; set; }
    }
}
