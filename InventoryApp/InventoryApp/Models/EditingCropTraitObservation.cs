﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class EditingCropTraitObservation : BindableBase
    {
        public int CropTraitId { get; set; }
        private string _cropTraitName;
        public string CropTraitName
        {
            get { return _cropTraitName; }
            set { SetProperty(ref _cropTraitName, value); }
        }
        private string _displayValue;
        public string DisplayValue
        {
            get { return _displayValue; }
            set { SetProperty(ref _displayValue, value); }
        }
        public bool IsCoded { get; set; }
        public string DataType { get; set; }
        public IEnumerable<CropTraitCode> CropTraitCodeList { get; set; }
        private CropTraitCode _selectedCropTraitCode;
        public CropTraitCode SelectedCropTraitCode
        {
            get { return _selectedCropTraitCode; }
            set { SetProperty(ref _selectedCropTraitCode, value); }
        }
    }
}
