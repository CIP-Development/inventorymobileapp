﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Enums
{
    public enum CropTraitDataTypeEnum
    {
        Char,
        Lower,
        Upper,
        Numeric
    }
    public static class CropTraitDataTypeExtensions
    {
        public static string GetString(this CropTraitDataTypeEnum enumeration)
        {
            switch (enumeration)
            {
                case CropTraitDataTypeEnum.Char:
                    return "CHAR";
                case CropTraitDataTypeEnum.Lower:
                    return "LOWER";
                case CropTraitDataTypeEnum.Upper:
                    return "UPPER";
                case CropTraitDataTypeEnum.Numeric:
                    return "NUMERIC";
                default:
                    throw new Exception("Not supported data type");
            }
        }
    }
}
